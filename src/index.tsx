import React from 'react';
import ReactDOM from 'react-dom';
import Screens from './screens';

ReactDOM.render(
  <React.StrictMode>
    <Screens />
  </React.StrictMode>,
  document.getElementById('root')
);
