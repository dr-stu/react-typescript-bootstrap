# Quick Start

Local development:

- npm i
- npm test
- npm start
- http://localhost:3000

Run all checks before raising a pull request:

- npm run pr

# Directory structure

- "/src/components" - The reusable components designed to be inserted in any screens.
- "/src/screens" - The non-reusable components representing the page structure/hierarchy.
